package employee.and.manager;

public class Manager extends Employee {
    private double bonus;
     
     
     Manager(){
          
     }
     
     Manager(String name, double hourlyWage, double hoursWorked, double bonus){
       super(name,hourlyWage, hoursWorked);
          this.bonus = bonus;
     }
 
     Manager(double hourlyWage, double hoursWorked, double bonus){
        super(hourlyWage, hoursWorked);
          this.bonus = bonus;
     }
     
     public double calculatePay() {
          return (super.calculatePay()) + bonus;

        }       
}


package employee.and.manager;

public class Employee {
    
    private String name;
     private double hourlyWage;
     private double hoursWorked;
     
     Employee(){
          
     }
     
     Employee(String name, double hourlyWage, double hoursWorked){
          this.name = name;
          this.hourlyWage = hourlyWage;
          this.hoursWorked = hoursWorked;
     }
     
      Employee(double hourlyWage, double hoursWorked){
          this.hourlyWage = hourlyWage;
          this.hoursWorked = hoursWorked;
     }

     public double calculatePay() {
          return hourlyWage * hoursWorked;
     }
 
     public String getName() {
          return name;
     }
 
     public double getHourlyWage() {
          return hourlyWage;
     }
 
     public double getHoursWorked() {
          return hoursWorked;
     }
}
 

    


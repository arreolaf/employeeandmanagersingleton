package employee.and.manager;

import static employee.and.manager.EmployeeType.EMPLOYEE;
import static employee.and.manager.EmployeeType.MANAGER;

public class EmployeeFactory {
    
    private static EmployeeFactory instance;
    
    private EmployeeFactory(){}
    
    public static EmployeeFactory getInstance(){
        if(instance == null){
        instance = new EmployeeFactory();
        }
        return instance;
    }
    
    public static Employee getTotalWage(EmployeeType type , double hourlyWage, double hoursWorked,double bonus){
        if(type == EMPLOYEE){
            return new Employee(hourlyWage,hoursWorked);
        }else if(type==MANAGER){
            return new Manager(hourlyWage,hoursWorked,bonus);
        }
        
        return null;
    }
    
}

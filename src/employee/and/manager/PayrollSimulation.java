
package employee.and.manager;

public class PayrollSimulation {

    public static void main(String[] args) {
     EmployeeFactory factory = EmployeeFactory.getInstance();
     
     Employee emp = factory.getTotalWage(EmployeeType.EMPLOYEE,20.0,40.0,0);
     Employee man = factory.getTotalWage(EmployeeType.MANAGER,35.0,40.0,500.0);
     
     
     
        System.out.println("The weekly pay for a normal employee is: " +"$" + emp.calculatePay());
        System.out.println("The weekly pay for a manager is: " +"$" + man.calculatePay());
     
     
    }
    
}
